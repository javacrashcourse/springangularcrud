/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bg.tilchev.springangularcrud.services;

import bg.tilchev.springangularcrud.entities.User;
import java.util.List;

/**
 *
 * @author todor.ilchev
 */
public interface UserService {
    
    User create(User user);

    User delete(long id);

    List<User> findAll();

    User findById(long id);
}
