/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bg.tilchev.springangularcrud.services;

import bg.tilchev.springangularcrud.entities.User;
import bg.tilchev.springangularcrud.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 *
 * @author todor.ilchev
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(User user) {
        return this.userRepository.saveAndFlush(user);
    }

    @Override
    public User delete(long id) {
        User user = findById(id);
        User userCopy = new User();
        userCopy.setEmail(user.getEmail());
        userCopy.setFirstName(user.getFirstName());
        userCopy.setLastName(user.getLastName());
        userCopy.setId(user.getId());
        if (user != null) {
            this.userRepository.delete(user);
        }
        return userCopy;
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User findById(long id) {
        return this.userRepository.getOne(id);
    }
}
