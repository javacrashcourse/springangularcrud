package bg.tilchev.springangularcrud;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import bg.tilchev.springangularcrud.entities.User;
import bg.tilchev.springangularcrud.repositories.UserRepository;
import bg.tilchev.springangularcrud.services.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTests {

    private UserServiceImpl userService;
    
    @Mock
    private UserRepository userRepo;
    
    private User expectedUser;
    
    @Before
    public void setup() {
        userRepo = Mockito.mock(UserRepository.class);
        expectedUser = new User();
        expectedUser.setEmail("kurdanov@mail.mail");
        Mockito.when(userRepo.getOne(ArgumentMatchers.anyLong())).thenReturn(expectedUser);
        this.userService = new UserServiceImpl(userRepo);
    }
    
    @Test
    public void findByIdShouldCallUserRepoGetOneAndReturnExpectedUser() {
        User actual = this.userService.findById(5L);
        Mockito.verify(userRepo, Mockito.times(1));
        Assert.assertEquals(expectedUser.getEmail(), actual.getEmail());
    }
}
